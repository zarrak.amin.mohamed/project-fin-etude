<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\utilisateur;

class logincontroller extends Controller
{
    public function login(Request $request)
    {
        // Validate the request data
        $validatedData = $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        // Check if the user already exists
        $existingUser = utilisateur::where('username', $validatedData['username'])->first();
        if ($existingUser) {
            return response()->json(['message' => 'User already exists'], 409);
        }

        // Insert data into the database
        $user = utilisateur::create([
            'username' => $validatedData['username'],
            'password' => bcrypt($validatedData['password']), // Hash the password
        ]);

        return response()->json(['message' => 'Data inserted successfully'], 201);
    }
}
