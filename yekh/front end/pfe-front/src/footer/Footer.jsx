import React from "react";

function Footer() {
  return (
    <footer className="bg-gray-900 text-white">
      <div className="container mx-auto py-8 px-4">
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
          {/* About Us */}
          <div className="text-center md:text-left">
            {" "}
            {/* Center text on small screens, left align on md screens */}
            <h2 className="text-lg font-semibold mb-4">About Us</h2>
          </div>

          {/* Quick Links */}
          <div className="text-center md:text-left">
            {" "}
            {/* Center text on small screens, left align on md screens */}
            <h2 className="text-lg font-semibold mb-4">Quick Links</h2>
            <ul>
              <li>
                <a href="#" className="text-sm hover:text-gray-400 block">
                  Home
                </a>
              </li>
              <li>
                <a href="#" className="text-sm hover:text-gray-400 block">
                  About
                </a>
              </li>
              <li>
                <a href="#" className="text-sm hover:text-gray-400 block">
                  Services
                </a>
              </li>
              <li>
                <a href="#" className="text-sm hover:text-gray-400 block">
                  Contact
                </a>
              </li>
            </ul>
          </div>

          {/* Contact Us */}
          <div className="text-center md:text-left">
            {" "}
            {/* Center text on small screens, left align on md screens */}
            <h2 className="text-lg font-semibold mb-4">Contact Us</h2>
            <p className="text-sm">123 Main Street, City</p>
            <p className="text-sm">info@example.com</p>
            <p className="text-sm">123-456-7890</p>
            <div className="mt-4 flex gap-4 justify-center md:justify-start">
              {" "}
              {/* Center flex items on small screens, left align on md screens */}
              <a href="#" className="text-gray-400 hover:text-white">
                <ion-icon name="logo-instagram" className="text-6xl"></ion-icon>
              </a>
              <a href="#" className="text-gray-400 hover:text-white">
                <ion-icon name="logo-youtube" className="text-6xl"></ion-icon>
              </a>
              <a href="#" className="text-gray-400 hover:text-white">
                <ion-icon name="logo-facebook" className="text-6xl"></ion-icon>
              </a>
            </div>
          </div>
        </div>

        {/* Copyright */}
        <div className="border-t border-gray-800 mt-8 pt-4 text-sm text-gray-600 text-center">
          <p>&copy; 2024 Your Company. All rights reserved.</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
