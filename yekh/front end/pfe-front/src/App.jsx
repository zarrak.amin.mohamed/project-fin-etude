// App.js
import React from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nopage from "./pages/Nopage";
import About from "./pages/About";
import Blogs from "./pages/Blogs";
import Layout from "./pages/Layout";
import Login from "./pages/Login";
import Service from "./Service";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />} />
        <Route path="/about" element={<About />} />
        <Route path="/blogs" element={<Blogs />} />
        <Route path="/services" element={<Service />} />
        <Route path="/api/login" element={<Login />} />
        <Route path="*" element={<Nopage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
