import React, { useState } from "react";
import { Link } from "react-router-dom"; // Import Link from react-router-dom
import Button from "./Button";

function Navbar() {
  let links = [
    { name: "HOME", link: "/" },
    { name: "SERVICE", link: "/services" }, // Example of different routes
    { name: "ABOUT", link: "/about" }, // Example of different routes
    { name: "BLOG'S", link: "/blogs" }, // Example of different routes
    { name: "CONTACT", link: "/contact" },
    // Example of different routes
  ];
  let [open, setOpen] = useState(false);
  return (
    <div className="shadow-md w-full fixed top-0 left-0">
      <div className="md:flex items-center justify-between bg-white py-4 md:px-10 px-7">
        <div className="font-bold text-2xl cursor flex items-center text-gray-800">
          <span className="text-3xl text-blue-600 mr-1 pt-2">
            <ion-icon name="airplane-sharp"></ion-icon>
          </span>
          TimePlanner
        </div>

        <div
          onClick={() => setOpen(!open)}
          className="text-3xl absolute right-8 top-6 cursor-pointer md:hidden"
        >
          <ion-icon name="menu-sharp"></ion-icon>
        </div>
        <ul
          className={`md:flex md:items-center md:pb-0 pb-12 absolute md:static bg-white md:z-auto z-[-1] left-0 w-full md:w-auto md:pl-0 pl-9 transition-all duration-500 ease-in ${
            open ? "top-20 opacity-100" : "top-[-490px] opacity-100"
          }`}
        >
          {links.map(
            (
              link,
              index // Change variable name from Link to link
            ) => (
              <li key={index} className="md:ml-8 text-xl md:my-0 my-7">
                {/* Use Link instead of anchor tag */}
                <Link
                  to={link.link} // Use link.link instead of Link.link
                  className="text-gray-800 hover:text-gray-400 duration-500"
                >
                  {link.name} {/* Use link.name instead of Link.name */}
                </Link>
              </li>
            )
          )}
          <Link to="/api/login">
            {" "}
            <Button> Sign in</Button>
          </Link>
        </ul>
      </div>
    </div>
  );
}

export default Navbar;
