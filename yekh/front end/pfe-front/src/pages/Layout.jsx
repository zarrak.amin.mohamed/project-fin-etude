import React from "react";
import Navbar from "./nav/Navbar";
import Home from "./Home";
import Footer from "../footer/Footer";

function Layout() {
  return (
    <>
      <Navbar />
      <br />
      <br />
      <br />

      <Home />
      <Footer />
    </>
  );
}

export default Layout;
