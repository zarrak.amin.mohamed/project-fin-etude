import React from "react";

function Home() {
  const city = [
    {
      country: "United States",
      capital: "Washington, D.C.",
      population: "702455",
      src: "/pictures/images.jpeg", // Removed "public/" from the source path
    },
    {
      country: "United Kingdom",
      capital: "London",
      population: "8982000",
      src: "/pictures/download (1).jpeg",
    },
    {
      country: "France",
      capital: "Paris",
      population: "2141000",
      src: "/pictures/download.jpeg",
    },
    {
      country: "Germany",
      capital: "Berlin",
      population: "3562000",
      src: "/pictures/download (2).jpeg",
    },
    {
      country: "China",
      capital: "Beijing",
      population: "21540000",
      src: "/pictures/download (3).jpeg",
    },
    {
      country: "France",
      capital: "Paris",
      population: "2141000",
      src: "/pictures/download.jpeg",
    },
  ];

  return (
    <div>
      {/* Hero Section */}
      <div className="bg-blue-900 text-white py-16">
        <div className="container mx-auto text-center">
          <h1 className="text-4xl font-bold mb-4">Welcome to TimePlanner</h1>
          <p className="text-lg mb-8">Where innovation meets excellence.</p>
          <a
            href="#features"
            className="bg-white text-blue-900 px-6 py-3 rounded-full font-bold uppercase hover:bg-blue-800 hover:text-white transition duration-300"
          >
            Learn More
          </a>
        </div>
      </div>

      {/* Features Section */}

      <div id="features" className="py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-3xl font-bold mb-8">Best Places to visit</h2>
          <div className="flex flex-wrap justify-center">
            {city.map((ct, index) => (
              <div
                key={index}
                className="w-full sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/5 px-4 mb-8"
              >
                <div className="bg-gray-100 rounded-lg p-6">
                  <div className="flex justify-center items-center">
                    <div
                      className="flex justify-center items-center"
                      style={{ height: "200px" }}
                    >
                      <img
                        src={ct.src}
                        className="w-full h-full object-cover rounded-lg"
                        alt="Your Image"
                      />
                    </div>
                  </div>

                  <h3 className="text-xl font-semibold mb-2">{ct.country}</h3>
                  <p>Capital: {ct.capital}</p>
                  <p>Population: {ct.population}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>

     
    {/* Testimonials Section */}
<div className="bg-gray-100 py-16">
  <div className="container mx-auto text-center">
    <h2 className="text-3xl font-bold mb-8">Testimonials</h2>
    <div className="flex flex-wrap justify-center">
      {/* Testimonial 1 */}
      <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 xl:w-1/4 px-4 mb-8 hover:scale-105 ">
        <div className="bg-white rounded-lg p-6 shadow-md h-64 flex flex-col justify-between">
          <div className="mb-4">
            <img
              src="/pictures/download (2).jpeg" // Replace with path to client's image
              className="w-16 h-16 object-cover rounded-full mx-auto"
              alt="fahd ait driss"
            />
          </div>
          <div>
            <h3 className="text-lg font-semibold mb-2">fahd ait driss</h3>
            <p className="text-sm mb-2">"The service was outstanding and the experience was unforgettable!"</p>
            <p className="text-gray-600">- Client Position, Company</p>
          </div>
        </div>
      </div>

      {/* Testimonial 2 */}
      <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 xl:w-1/4 px-4 mb-8 hover:scale-105 " >
        <div className="bg-white rounded-lg p-6 shadow-md h-64 flex flex-col justify-between">
          <div className="mb-4">
            <img
              src="/pictures/download (1).jpeg" 
              className="w-16 h-16 object-cover rounded-full mx-auto"
              alt="mohamed amine"
            />
          </div>
          <div>
            <h3 className="text-lg font-semibold mb-2">mohamed amine</h3>
            <p className="text-sm mb-2">"The experience was unforgettable!"</p>
            <p className="text-gray-600">- Client Position, Company</p>
          </div>
        </div>
        
      </div>
        {/* Testimonial 3 */}
<div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 xl:w-1/4 px-4 mb-8 hover:scale-105 ">
        <div className="bg-white rounded-lg p-6 shadow-md h-64 flex flex-col justify-between">
          <div className="mb-4">
            <img
              src="\public\pictures\nejjar.png" 
              className="w-16 h-16 object-cover rounded-full mx-auto"
              alt="nejjar"
            />
          </div>
          <div>
            <h3 className="text-lg font-semibold mb-2">nejjar</h3>
            <p className="text-sm mb-2">"apah apah apah rihla wa3ra apah apah kososan khbiza apah apah  !"</p>
            <p className="text-gray-600">- si 3li jamae mezwak, tetouan</p>
          </div>
        </div>
      </div>

    </div>
    
  </div>



</div>




      {/* Call to Action Section */}
      <div className="bg-blue-900 text-white py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-3xl font-bold mb-4">Ready to get started?</h2>
          <p className="text-lg mb-8">Sign up now and unleash the power!</p>
          <a
            href="#contact"
            className="bg-white text-blue-900 px-6 py-3 rounded-full font-bold uppercase hover:bg-blue-800 hover:text-white transition duration-300"
          >
            Contact Us
          </a>
        </div>
      </div>
    </div>
  );
}

export default Home;
