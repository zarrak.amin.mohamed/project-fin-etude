import React from 'react';

const HotelCard = ({ hotel }) => (
  <div className="max-w-sm rounded overflow-hidden shadow-lg bg-white">
    <img className="w-full h-48 object-cover" src={hotel.img} alt={`img ${hotel.name}`} />
    <div className="px-6 py-4">
      <div className="font-bold text-xl mb-2">{hotel.name}</div>
      <p className="text-gray-700 text-base">
        rating: <span className="font-bold">{hotel.rating}</span>
      </p>
    </div>
  </div>
);

export default HotelCard;
