import React from 'react';
import Navbar from './nav/Navbar';

export default function About() {
  return (
    <>
      <Navbar />
      <div className="container mx-auto p-4">
        <div className="bg-white text-gray-600 body-font">
          <div className="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
            <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 md:mb-0 mb-10">
              <img className="object-cover object-center rounded" alt="hero" src="\public\pictures\travel.png" />
            </div>
            <div className="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">About Us</h1>
              <p className="mb-8 leading-relaxed">Here at TravelSite, we're dedicated to making travel effortless and exciting for all our customers.</p>
              <div className="flex justify-center">
                <button className="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Learn More</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
