import React from "react";
import Navbar from "./nav/Navbar";
import HotelCard from "./hotels/HotelCard";

function Blogs() {
  const hotelsData = [
    {
      id: 1,
      name: 'paloma' ,
      rating: 4.5,
      img: '/public/pictures/la-paloma.jpg'
    },
    {
      id: 2,
      name: 'hotel o keda' ,
      rating: 4,
      img: '/public/pictures/photo-hotel-paris.jpg'
    },
    {
      id: 3,
      name: 'hotel mohim' ,
      rating: 4.5,
      img: '/public/pictures/photo-hotel-paris.jpg'
    },
    {
      id: 4,
      name: 'hadana' ,
      rating: 5,
      img: '/public/pictures/photo-hotel-paris.jpg'
    },
    {
      id: 5,
      name: 'hotel mohim' ,
      rating: 4.5,
      img: '/public/pictures/photo-hotel-paris.jpg'
    },

    {
      id: 6,
      name: 'hotel mohim' ,
      rating: 4.5,
      img: '/public/pictures/photo-hotel-paris.jpg'
    },
    
    


  ];

  return (
    <>
      {/* Background video */}
      <div className="fixed top-0 left-0 w-full h-full min-h-screen z-0 overflow-hidden">
        <video autoPlay loop muted className="w-full h-full object-cover">
          <source src="\public\pictures\Royalty Free 4k Forest Stock Video Footage [ DJI Drone Footage ].mp4" type="video/mp4" />
        </video>
      </div>

      {/* Content */}
      <div className="relative z-10">
        <Navbar/> 
        <div className="container mx-auto pt-24">
          <div className="grid grid-cols-1 md:grid-cols-3 gap-4 justify-items-center">
            {hotelsData.map(hotel => (
              <div className="w-64">
                
                <HotelCard key={hotel.id} hotel={hotel} />
                
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default Blogs;
